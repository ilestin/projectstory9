from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
import requests
from .models import Favorite
# Create your views here.


def index(request):
    if request.method == 'POST':
        title = request.POST['title']
        id = request.POST['id']
        hasil = Favorite(title=title, id=id)
        hasil.save()
        count = Favorite.objects.count()
        return JsonResponse({'hasil': hasil.title, 'count': count, 'id': hasil.id})
    favorite = Favorite.objects.all()
    count = Favorite.objects.count()
    return render(request, 'lab_9/home.html', {'favorite': favorite, 'count': count})


def getData(request):
    get = requests.get(
        "https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    hasil = get['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        data = Favorite.objects.filter(pk=dataId)
        if not data.exists():
            bool = False
        else:
            bool = True
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist})


def delete(request):
    if request.method == 'POST':
        id = request.POST['id']
        hasil = get_object_or_404(Favorite, pk=id)
        nama = hasil.title
        hasil.delete()
        count = Favorite.objects.count()
        return JsonResponse({'count': count, 'name': nama})
