$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "/getdata",
        success: function (hasil) {
            var data = hasil.data;
            for (var x = 0; x < data.length; x++) {
                var dataBook = data[x];
                var title = dataBook.title;
                var name = title.replace(/'/g, "\\'");
                if (dataBook.boolean) {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + dataBook.id + '\')">' + '<i class="del"></i></button>';
                } else {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' + name + '\', \'' + dataBook.id + '\')">' + '<i class="ic"></i></button>';
                }
                var html = '<tr class="table-info">' + '<td>' + button + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                    '<td>' + dataBook.published + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function (error) {
            alert("Halaman buku tidak ditemukan")
        }
    })
});

var addFavorite = function (title, id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: { title: title, id: id },
        success: function (hasil) {
            var button = '<button' + ' id="' + hasil.id + '"' + ' class="bttnn"' 
            + ' onClick="deleteFavorite(\'' + hasil.id + '\')">' + '<i class="del"></i></button>';
            $("#" + hasil.id).replaceWith(button);
            html = "<div id='" + hasil.id + "'>" + hasil.hasil + "</div>";
            $(".data").append(html);
            $(".count").replaceWith("<span class='count'>" + hasil.count + "</span>")
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};
var deleteFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/delete",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: { id: id },
        success: function (count) {
            var button = '<button' + ' id="' + id + '"' + ' class="bttnn"' +
             ' onClick="addFavorite(\'' +
              count.name.replace(/'/g, "\\'") + '\', \'' + id +
               '\')">' + '<i class="ic"></i></button>';
            $("tbody").find("#" + id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>" + count.count + "</span>");
            $("#" + id).remove();
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};