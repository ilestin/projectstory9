from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Favorite
import requests
# Create your tests here.


class Lab9UnitTest(TestCase):

    def test_lab9_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_favorite(self):
        Favorite.objects.create(title='a', id='abcd')
        count = Favorite.objects.count()
        self.assertEqual(count, 1)

    def test_client_can_POST_and_render(self):
        title = "hehehe"
        response_post = Client().post('/', {'title': title, 'id': 'abcd'})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)

    def test_client_can_delete_and_delete_selected(self):
        title = "hehehe"
        Client().post('/', {'title': title, 'id': title})
        response_post = Client().post('/delete', {'id': title})
        self.assertEqual(response_post.status_code, 200)
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(title, html_response)

    def test_client_cannot_delete_if_no_id_match(self):
        title = "iihhh"
        Client().post('/', {'title': title, 'id': title})
        response_post = Client().post('/delete', {'id': 'olala'})
        self.assertEqual(response_post.status_code, 404)

    def test_client_can_get_data(self):
        response = Client().get('/getdata')
        self.assertEqual(response.status_code, 200)

    def test_books_already_favorit(self):
        get = requests.get(
            "https://www.googleapis.com/books/v1/volumes?q=quilting").json()
        items = get['items']
        book = items[0]
        dataId = book['id']
        title = book['volumeInfo']['title']
        Client().post('/', {'title': title, 'id': dataId})
        response = Client().get('/getdata').json()
        bool = response['data'][0]['boolean']
        self.assertTrue(bool)

    def test_books_not_favorited(self):
        response = Client().get('/getdata').json()
        bool = response['data'][0]['boolean']
        self.assertFalse(bool)
